const express = require('express');
const app = express();
const PORT = 3000 || process.env.PORT;

const Axios = require('axios');

app.get('/', (req, res) => {
  res.send('<h1>Hello World</h1>');
});

app.get('/test', (req, res) => {
  Axios.get('http://api:3000/posts').then(response => res.send(response.data));
});


app.listen(PORT, () => console.log(`The app is listening to port ${PORT}`));
